### Install Guide

1. Download the latest Leaflet: Zoom home plugin from [here](https://github.com/torfsen/leaflet.zoomhome/archive/master.zip).
2. Extract it to the libraries folder and **rename the folder to leaflet_zoomhome.**
3. Enable the leaflet_zoomhome module.

You should see that the Zoom home plugin replaced the default zoom controls on all maps.
If this is not happened, then you should check the status of the "Leaflet: Zoom home" on the admin/reports/status page.